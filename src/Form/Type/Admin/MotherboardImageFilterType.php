<?php

namespace App\Form\Type\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MotherboardImageFilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'Schema only' => 'schema',
                'Schema and photo' => 'schemaphoto',
                'Photo only' => 'photo',
                'None' => 'none',
            ],
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}
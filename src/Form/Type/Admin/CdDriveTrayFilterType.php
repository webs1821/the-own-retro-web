<?php

namespace App\Form\Type\Admin;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CdDriveTrayFilterType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'Tray' => 'Tray',
                'Caddy' => 'Caddy',
                'Slot' => 'Slot'
            ],
        ]);
    }

    public function getParent()
    {
        return ChoiceType::class;
    }
}